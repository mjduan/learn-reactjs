/*
  @author dmj1161859184@126.com 2019-03-01 22:30
  @version 1.0
  @since 1.0
*/
import React from 'react';
import {HashRouter as Router,Route,Link} from 'react-router-dom';
import Home from '../components/Home';
import Topics from '../components/Topics'
import About from '../components/About'

class MyRouter extends React.Component{
    render(){
        return(
            <Router>
                <div>
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/about">About</Link></li>
                        <li><Link to="/topics">Topics</Link></li>
                    </ul>
                    <hr/>
                    <Route exact path="/" component={Home}/>
                    <Route path="/about" component={About}/>
                    <Route path="/topics" component={Topics}/>
                </div>
            </Router>
        )
    }
}

export default MyRouter;
