/*
  @author dmj1161859184@126.com 2019-03-01 23:03
  @version 1.0
  @since 1.0
*/
import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import Topic from './Topic';
class Topics extends React.Component{
    render(){
        console.log(this);
        return(
            <div>
                <h2>Topics</h2>
                <ul>
                    <li>
                        <Link to={'${this.props.match.url}/rendering'}>
                            Rendering with react
                        </Link>
                    </li>
                    <li>
                        <Link to={'${this.props.match.url}/components'}>
                            Components
                        </Link>
                    </li>
                    <li>
                        <Link to={'${this.props.match.url}/props-v-state'}>
                            Props v.state
                        </Link>
                    </li>
                </ul>
                <Route path={'${this.props.match.url}/:topicId'} component={Topic}/>
                <Route exact path={this.props.match.url} render={()=>(
                    <h3>Please select a topic.</h3>
                )} />
            </div>
        )
    }
}
export default Topics;
